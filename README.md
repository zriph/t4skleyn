README
=======
# tæskleyn

A superior sequel to wrktræk: A simple command-line work-tracking system that can easily output an HTML page ready for publishing.

# instructions

To report a task, type `perl tl.pl 0 0 0` - the first number is the hours you spent on a task, the second number is your morale (0-9), and the third number is the task's priority (0-9).

Multiple tasks reported in the same day will have their hours summed, and their "morale" and "priority" values combined via weighted-average.

To insert a text note, type `perl tl.pl note blah blah blah`. Notes can be arbitrarily long, but if they're *very* long they can become awkward to view in the HTML output-page.

To update the HTML page, type `perl tl.pl update`.

Filename preferences are kept in `prefs.txt`.

#!/usr/bin/perl
use warnings;
use strict;



use File::Copy; # Library for copying files (this is for making safety-backups of data)
use Time::Piece; # Library for simple date-and-time



my $datafile = "=NONE FOUND=";
my $outfile = "=NONE FOUND=";
my $themefile = "=NONE FOUND=";

my @lines = ();

my @months = qw(x Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);



# Take a number or string, check its length, and if it has length of 1, give it a single leading zero
sub datepad {
  return (((length $_[0]) < 2) ? "0" : "") . $_[0];
}

# Turn a specific type of ISO-8601-compatible date-format into a more "cool-looking" human-readable format
sub cooldate {
  my @a = split "-", $_[0]; # Split the ISO-8601-compatible date-format into year, month, day values
  return $a[0] . " " . $months[$a[1]] . $a[2]; # Convert the date-values to a cool-to-read date format, and return the value
}

# Build an ISO-8601-compatible date-and-time phrase (this will allow it to be comparable via "lt, gt" operators elsewhere)
sub getdate {
  my $t = localtime; # Get the current date-and-time information
  return $t->year . "-" . datepad($t->mon) . "-" . datepad($t->mday); # Format the date information into "year-month-day", adding leading-zeroes as needed
}



if ((scalar @ARGV) == 0) { # If the program received no args...
  die("error: did not receive any arguments!\n"); # Die with a relevant error message
}



print "loading info from prefs.txt...\n";

open my $IN_PREFS, '<', 'prefs.txt' or die "error: can't open prefs.txt!\n"; # Open the prefs-file

while (my $rowstr = <$IN_PREFS>) { # For every line in the prefs-file...
  
  my @row = split " ", $rowstr; # Split the row into an array
  
  my $cmd = lc $row[0]; # Get the lowercase version of the first item in the line
  
  if ($cmd =~ m/data/) { # If this row is a "data-file" setting...
    $datafile = $row[1]; # Grab the given data-file name
  } elsif ($cmd =~ m/theme/) { # Else, if this row is a "theme-file" setting...
    $themefile = $row[1]; # Grab the given theme-file name
  } elsif ($cmd =~ m/output/) { # Else, if this row is an "output-file" setting...
    $outfile = $row[1]; # Grab the given output-file name
  }
  
}

close $IN_PREFS; # Close the prefs-file

print "preferences and filenames loaded.\n";

# Check whether the filenames for all relevant files have been found in prefs.txt, else die
if ($datafile eq "=NONE FOUND=") { die "error: entry for 'data-file' not found in prefs.txt!\n"; }
if ($outfile eq "=NONE FOUND=") { die "error: entry for 'output-file' not found in prefs.txt!\n"; }
if ($themefile eq "=NONE FOUND=") { die "error: entry for 'theme-file' not found in prefs.txt!\n"; }

if (!(-f $datafile)) { # If the datafile doesn't already exist...
  
  print "creating file $datafile...\n";
  
  # Create the user-defined datafile, but don't put any data into it
  open my $OUT_NEWDATA, '>', $datafile or die("error: could not create $datafile!\n");
  close $OUT_NEWDATA;
  
  print "$datafile created.\n";
  
}



if (lc($ARGV[0]) eq "update") { # If this is an "update the HTML file" command...
  
  print "updating HTML...\n";
  
  my $out = "";
  
  print "parsing data-file '$datafile'...\n";
  
  open my $IN_DATA, '<', $datafile or die("error: could not open data-file '$datafile'!\n");
  
  while (my $rowstr = <$IN_DATA>) { # For every line in the data-file...
    
    my @row = split " ", $rowstr; # Split the row into an array, by space
    
    if ((scalar @row) == 0) { next; } # If this line contains no items, go to the while-loop's next iteration
    
    # At this point, it is confirmed that @row contains at least one item.
    
    if ($row[1] =~ m/note/) { # If this row contains "text note" data...
      push @lines, "<details><summary>" . cooldate($row[0]) . "</summary>" . join(' ', @row[2..$#row]) . "</details>"; # Format this line in the "note"-style
      next; # Go to the while-loop's next iteration
    }
    
    # Having checked for empty lines and note-lines,
    # the only remaining valid possibility is a work-line, which should start with an hours-number. So...
    
    if ($row[1] !~ m/^\d+(\.\d+)?$/) { next; } # If this line's first non-date item is neither a positive float nor a positive integer, go to the while-loop's next iteration
    
    for (1 .. 3) { $row[$_] = int($row[$_] + 0.5); } # Convert any float-values in the work-data into their nearest integers
    
    # Create an HTML object containing the date and the work-data
    push @lines, "<p class=\"hou$row[1]\"><mor$row[2]></mor$row[2]><pri$row[3]></pri$row[3]><br/>" . cooldate($row[0]) . "</p>";
    
  }
  
  close $IN_DATA; # Close the data-file
  
  print "done parsing data-file '$datafile'.\n";
  
  print "parsing theme-file '$themefile'...\n";
  
  open my $IN_THEME, '<', $themefile or die("error: could not open theme-file '$themefile'!\n");
  
  while (my $thstr = <$IN_THEME>) { # For every line in the theme-file...
    if (index($thstr, "<!-- tæskleyn -->") != -1) { # If this line contains tæskleyn's activation-phrase...
      $out .= (join "\n", @lines) . "\n"; # Add all of the processed lines from the datafile-related routines
    } else { # Else, if this line doesn't contain adotrækr's activation-phrase...
      $out .= $thstr; # Add the line to the output-string without any modifications
    }
  }
  
  close $IN_THEME; # Close the theme-file
  
  print "done parsing theme-file '$themefile'.\n";
  
  print "opening output-file '$outfile'...\n";
  
  open my $OUT_COMPOSITE, '>', $outfile or die("error: could not open output-file '$outfile'!\n");
  print $OUT_COMPOSITE "$out"; # Print the entire output-string to the target-file
  close $OUT_COMPOSITE; # Close the target-file
  
  print "done updating output-file '$outfile'.\n";
  
} elsif (lc($ARGV[0]) eq "note") { # Else, if this is a "log a text note" command...
  
  # If the note is empty, quit the program
  if ((scalar @ARGV) == 1) { die "error: note did not contain anything!\n"; }
  
  my $date = getdate(); # Get an ISO-8601-compatible year-and-date string for the current day
  
  my $found = 0; # Flag for "has an entry from a current or previous day been found already?"
  
  open my $IN_WHOLE, '<', $datafile or die("error: could not open data-file '$datafile'!\n");
  open my $OUT_WHOLE, '>', "$datafile.tmp" or die("error: could not make temporary file '$datafile.tmp'!\n");
  
  while (my $rowstr = <$IN_WHOLE>) { # For every line in the data-file...
    
    my @row = split " ", $rowstr; # Split the row into an array, by space
    
    if (
      ((scalar @row) > 0) # If this isn't an empty line...
      && (!$found) # And an entry from a current or previous day has not yet been found...
      && ( # And...
        ($row[0] eq $date) # Either: the current entry is from the current date...
        || ($row[0] lt $date) # Or: the current entry is from a past date...
      )
    ) {
      print $OUT_WHOLE ($date . " " . (join ' ', @ARGV) . "\n"); # Put the given text-note into the datafile immediately before the current entry
      $found = 1; # Raise the flag for "an entry from a current or previous day has been found"
    }
    
    print $OUT_WHOLE (join(' ', @row) . "\n"); # Put the already-existing line into the datafile
    
  }
  
  if (!$found) { # If an entry from a current or previous day was never found...
    print $OUT_WHOLE ($date . " " . (join ' ', @ARGV) . "\n"); # Put the given text-note into the datafile
  }
  
  close $IN_WHOLE;
  close $OUT_WHOLE;
  
  copy("$datafile.tmp", $datafile) or die("error: could not copy $datafile.tmp into $datafile!\n");
  
  print "note added to data-file.\n";
  
} else { # Else, this is an "add data" command, so start parsing data for it...
  
  # If the given command doesn't contain correct args for "hours", "morale", and "priority", reject it
  if ((scalar @ARGV) != 3) { die "error: given command contains wrong number of arguments!\n"; }
  
  for (0 .. 2) { # For each of the 3 data-arguments (time, morale, priority)...
    # If the argument is neither an integer nor a float, or if it's negative, then exit the program
    if ($ARGV[$_] !~ m/^\d+(\.\d+)?$/) { die "error: " . $ARGV[$_] . " is not a valid number!\n"; }
  }
  
  # If the given number of hours is 0, reject it
  if ($ARGV[0] == 0) { die "error: given number of hours was below 1!\n"; }
  
  # For both "morale" and "priority", if they are above 9, reduce them to 9
  for (1 .. 2) { ($ARGV[$_] > 9) ? ($ARGV[$_] = 9) : (); }
  
  if (-f ($datafile . "-backup.txt")) { # If the first-stringer backup-file already exists...
    
    print "making secondary backups of $datafile...\n";
    
    # Copy the first-stringer backup-file into the second-stringer backup-file
    copy($datafile . "-backup.txt", $datafile . "-backup-old.txt") or die("error: could not copy " . $datafile . "-backup.txt into " . $datafile . "-backup-old.txt!\n");
    
  }
  
  print "making backups of $datafile...\n";
  
  # Copy the main datafile into the first-stringer backup-file
  copy($datafile, $datafile . "-backup.txt") or die("error: could not copy $datafile into " . $datafile . "-backup.txt!\n");
  
  my $date = getdate(); # Get an ISO-8601-compatible year-and-date string for the current day
  
  my $found = 0; # Flag: "we already found a position to insert the given data"
  
  print "adding data to $datafile.tmp...\n";
  
  # Open the data-file for reading, and a temp-datafile for writing
  open my $IN_COMPARE, '<', $datafile or die("error: could not open $datafile!\n");
  open my $OUT_COMPARE, '>', "$datafile.tmp" or die("error: could not open $datafile.tmp!\n");
  
  while (my $fullrow = <$IN_COMPARE>) { # For every line in the data-file...
    
    my @row = split " ", $fullrow; # Split this linw into an array, by space
    
    if ((scalar @row) == 0) { next; } # If this line contains no items, go to the while-loop's next iteration
    
    if ($row[1] =~ m/note/) { # If this row contains text-note data...
      
      if (
        (!$found) # If an insertion-position has not yet been found...
        && ($row[0] lt $date) # And this text-note is from a previous day...
      ) {
      
        print $OUT_COMPARE "$date $ARGV[0] $ARGV[1] $ARGV[2]\n"; # Print the new data into the file
        
        $found = 1; # Raise the flag for "we found an insertion-point for today's data"
        
      }
      
    } elsif (!$found) { # Else, if this is a work-data row, and an insertion-point has not yet been found...
      
      if ($row[0] eq $date) { # If this row is from the current day...
        
        # We need to recombine today's already-existing data with the user-submitted data. So let's do that!
        
        my $hours = $row[1] + $ARGV[0]; # Combine the total hours from both the new entry and the already-existing entry
        
        # Calculate the weighted average for both "morale" and "priority", based on the hours given
        for (2 .. 3) { $row[$_] = (($row[1] * $row[$_]) + ($ARGV[0] * $ARGV[$_ - 1])) / $hours; }
        
        print $OUT_COMPARE "$date $hours $row[2] $row[3]\n"; # Print the composite data back into the file
        
        $found = 1; # Raise the flag for "we found an insertion-point for today's data"
        
        next; # Skip the rest of the while-loop, so the line that was combined with the given data doesn't get double-printed
        
      } elsif ($row[0] lt $date) { # Else, if this row is from a previous day...
        
        print $OUT_COMPARE "$date $ARGV[0] $ARGV[1] $ARGV[2]\n"; # Print the new data into the file
        
        $found = 1; # Raise the flag for "we found an insertion-point for today's data"
        
      }
      
    }
    
    print $OUT_COMPARE $fullrow; # Print the row as-is
    
  }
    
  if (!$found) { # If the data-file had no entries (or only entries from the future[?!])...
    print $OUT_COMPARE "$date $ARGV[0] $ARGV[1] $ARGV[2]\n"; # Print the given data into the file
  }
  
  close $IN_COMPARE; # Close the datafile
  close $OUT_COMPARE; # Close the updated-temp-datafile
  
  print "data added to $datafile.tmp.\n";
  
  print "copying $datafile.tmp to $datafile...\n";
  
  # Copy the updated-temp-datafile into the regular datafile
  copy("$datafile.tmp", $datafile) or die("error: could not copy $datafile.tmp into $datafile!\n");
  
  print "$datafile.tmp copied to $datafile.\n";
  
}

print "operations successful. now exiting tl.pl.\n";


